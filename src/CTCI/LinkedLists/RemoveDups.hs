module CTCI.LinkedLists.RemoveDups where

import Control.Monad.ST
import Data.Hashable

import qualified Data.HashTable.ST.Basic as H

-- Write code to remove duplicates from an unsorted linked list.
-- Followup: How would you solve this problem if a temporary
-- buffer is not allowed?

-- | First solution uses a hashtable to check if an
-- element is a duplicate.
-- The time complexity is O(n).
removeDups :: (Hashable e, Eq e) => [e] -> [e]
removeDups l = runST $ do
    dict <- H.new
    go dict l
  where
    go _ [] = return []
    go dict (e:es) = do
        result <- H.lookup dict e
        case result of
            Just () -> go dict es
            Nothing -> do
                H.insert dict e ()
                rest <- go dict es
                return $ e:rest

-- | Second solution doesn't use a temporary buffer by looking ahead
-- and removing the duplicates for each element.
-- The time complexity is O(n^2).
removeDupsTwo :: (Eq e) => [e] -> [e]
removeDupsTwo []     = []
removeDupsTwo (e:es) = e:removeDupsTwo (removeDuplicates e es)
  where
    removeDuplicates _ [] = []
    removeDuplicates elem (e:es)
        | elem == e = removeDuplicates elem es
        | otherwise = e:removeDuplicates elem es
