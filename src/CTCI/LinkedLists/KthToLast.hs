module CTCI.LinkedLists.KthToLast where

-- Implement an algorithm to find the kth to last element of a singly linked list.

-- | Solution is to move up one pointer by k
-- and then move both pointers until the end
-- pointer hits the end. Then the start pointer
-- will be the kth to last node.
kthToLast :: Int -> [e] -> Maybe [e]
kthToLast k l = moveToEnd l =<< moveK k l
  where
    moveToEnd kth []        = Just kth
    moveToEnd (_:as) (_:bs) = moveToEnd as bs
    moveToEnd _ _           = Nothing

    moveK 0 l      = Just l
    moveK _ []     = Nothing
    moveK k (_:es) = moveK (k - 1) es
