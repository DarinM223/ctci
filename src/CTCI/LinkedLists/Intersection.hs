module CTCI.LinkedLists.Intersection where

-- Given two (singly) linked lists, determine if the two lists intersect.
-- Return the intersecting node. Note that intersection is defined by
-- reference, not value. That is, if the kth node of the first linked list
-- is the exact same node (by reference) as the jth node of the second
-- linked list, then they are intersecting.

-- How to test example:
-- > list1 <- newList [3, 1, 5, 9, 7, 2, 1]
-- > list2 <- newList [4, 6]
-- > (_, last) <- listLengthAndLastNode list2
-- > middle <- moveList 4 list1
-- > connectLists last middle
-- > intersect <- checkIntersection list1 list2
-- > printNode intersect
-- 7

import Control.Monad
import Data.IORef

-- Pointer equality can be done in Haskell using IORefs.
type ListRef a = IORef (LinkedList a)
type Node a = Maybe (ListRef a)

data LinkedList a = LinkedList
    { _data :: a
    , _next :: Node a
    }

newList :: [a] -> IO (Node a)
newList []     = return Nothing
newList (e:es) = do
    list <- LinkedList <$> pure e <*> newList es
    ref <- newIORef list
    return $ Just ref

connectLists :: Node a -> Node a -> IO ()
connectLists (Just second) first =
    modifyIORef second (\l -> l { _next = first })
connectLists Nothing _ = return ()

printNode :: (Show a) => Node a -> IO ()
printNode (Just ref) = do
    value <- readIORef ref
    print (_data value)
printNode Nothing = return ()

printList :: (Show a) => Node a -> IO ()
printList (Just ref) = do
    value <- readIORef ref
    print (_data value)
    printList (_next value)
printList Nothing = return ()

-- 1. Get the lengths and tails of each list.
-- 2. If tails are different, return immediately.
-- 3. Advance the node in the bigger list by the difference
-- in the list lengths.
-- 4. Move both list's nodes by 1 until both nodes are the same.
checkIntersection :: Node a -> Node a -> IO (Node a)
checkIntersection ref1 ref2 = do
    (length1, lastNode1) <- listLengthAndLastNode ref1
    (length2, lastNode2) <- listLengthAndLastNode ref2
    let (biggerNode, node2) = if length1 > length2
        then (ref1, ref2)
        else (ref2, ref1)
    if lastNode1 /= lastNode2
        then return Nothing
        else do
            node1 <- moveList (abs $ length2 - length1) biggerNode
            traverseLists node1 node2

listLengthAndLastNode :: Node a -> IO (Int, Node a)
listLengthAndLastNode = go Nothing
  where
    go :: Node a -> Node a -> IO (Int, Node a)
    go last Nothing = return (0, last)
    go _ (Just ref) = do
        next <- _next <$> readIORef ref
        (rest, last) <- go (Just ref) next
        return $ (1 + rest, last)

moveList :: Int -> Node a -> IO (Node a)
moveList n ref = foldM move ref [1..n]
  where
    move Nothing _    = return Nothing
    move (Just ref) _ = _next <$> readIORef ref

traverseLists :: Node a -> Node a -> IO (Node a)
traverseLists Nothing Nothing = return Nothing
traverseLists (Just ref1) (Just ref2)
    | ref1 == ref2 = return $ Just ref1
    | otherwise    = do
        next1 <- _next <$> readIORef ref1
        next2 <- _next <$> readIORef ref2
        traverseLists next1 next2
traverseLists _ _ = error "Mismatched lists"
