module CTCI.LinkedLists.Palindrome where

import Data.Maybe

-- Implement a function to check if a linked list is a palindrome.

isPalindrome :: (Eq e) => [e] -> Bool
isPalindrome l = isJust $ go (length l) l
  where
    go _ []     = Just []
    go 1 (_:es) = Just es
    go 2 (a:b:es)
        | a == b    = Just es
        | otherwise = Nothing
    go l (e:es) =
        case go (l - 2) es of
            Just (x:xs) | x == e -> Just xs
            _                    -> Nothing
