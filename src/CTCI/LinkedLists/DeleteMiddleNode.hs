module CTCI.LinkedLists.DeleteMiddleNode where

import Control.Monad
import Data.IORef

-- Implement an algorithm to delete a node in the middle
-- (i.e., any node but the first and last node, not necessarily
-- the exact middle) of a singly linked list, given only access
-- to that node.

-- Example:
-- Input: the node c from the linked list a -> b -> c -> d -> e -> f
-- Result: nothing is returned, but the new linked list looks like a -> b -> d -> e -> f

-- How to test example:
-- > import Data.Maybe
-- > l <- fromJust <$> linkedListFromList [1, 2, 3]
-- > linkedListToList l
-- [1,2,3]
-- > n <- fromJust <$> linkedListMoveN 1 l
-- > deleteMiddleNode n
-- > linkedListToList l
-- [1,3]

data LinkedList a = LinkedList
    { _data :: IORef a
    , _next :: IORef (Maybe (LinkedList a))
    }

-- | Sets the data to the next node's data
-- and the next node to the next next node.
deleteMiddleNode :: LinkedList a -> IO ()
deleteMiddleNode l = do
    next <- readIORef $ _next l
    mapM_ (writeIORef (_data l) <=< readIORef . _data) next
    mapM_ (writeIORef (_next l) <=< readIORef . _next) next

linkedListFromList :: [e] -> IO (Maybe (LinkedList e))
linkedListFromList [] = return Nothing
linkedListFromList (e:es) = do
    ref <- newIORef e
    next <- linkedListFromList es >>= newIORef
    return $ Just LinkedList { _data = ref, _next = next }

linkedListToList :: LinkedList e -> IO [e]
linkedListToList l = do
    v <- readIORef $ _data l
    next <- readIORef $ _next l
    case next of
        Just rest -> (v :) <$> linkedListToList rest
        _         -> return [v]

linkedListMoveN :: Int -> LinkedList e -> IO (Maybe (LinkedList e))
linkedListMoveN 0 l = return $ Just l
linkedListMoveN n l = do
    next <- readIORef $ _next l
    case next of
        Just next' -> linkedListMoveN (n - 1) next'
        Nothing    -> return Nothing
