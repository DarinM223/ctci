module CTCI.LinkedLists.LoopDetection where

-- Given a  circular linked list, implement and algorithm that returns the
-- node at the beginning of the loop.
--
-- Definition: Circular linked list: A (corrupt) linked list in which
-- a node's next pointer points to an earlier node, so as to make a loop in
-- the linked list.
--
-- Example:
-- Input: A -> B -> C -> D -> E -> C
-- Output: C

-- How to test example:
-- > l <- newList ["A", "B", "C", "D", "E"]
-- > mid <- moveList 2 l
-- > (_, last) <- listLengthAndLastNode l
-- > connectLists last mid
-- > node <- detectLoop l
-- > printNode node
-- "C"

import CTCI.LinkedLists.Intersection

detectLoop :: Node a -> IO (Node a)
detectLoop node = do
    node1 <- moveList 1 node
    node2 <- moveList 2 node
    collision <- findCollision 1 node1 2 node2
    findCollision 1 node 1 collision
  where
    findCollision moveA a moveB b
        | a == Nothing || b == Nothing = return Nothing
        | a == b                       = return a
        | otherwise                    = do
            a <- moveList moveA a
            b <- moveList moveB b
            findCollision moveA a moveB b
