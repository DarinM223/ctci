module CTCI.LinkedLists.SumLists
    ( sumLists
    , sumListsTwo
    ) where

-- You have two numbers represented by a linked list, where each node
-- contains a single digit. The digits are stored in reverse order,
-- that the 1's digit is at the head of the list. Write a function
-- that adds the two numbers and returns the sum as a linked list.

-- Example:
-- Input: (7 -> 1 -> 6) + (5 -> 9 -> 2). That is, 617 + 295.
-- Output: (2 -> 1 -> 9). That is, 912.

-- Followup: Suppose the digits are stored in forward order. Repeat the
-- above problem.

-- Example:
-- Input: (6 -> 1 -> 7) + (2 -> 9 -> 5). That is, 617 + 295.
-- Output: (9 -> 1 -> 2). That is, 912.

-- | Sums list in reverse order by keeping track of the carry.
sumLists :: [Int] -> [Int] -> [Int]
sumLists = go 0
  where
    go carry [] []
        | carry > 0 = [carry]
        | otherwise = []
    go carry es []         = go carry es [0]
    go carry [] es         = go carry [0] es
    go carry (a:as) (b:bs) = digit:rest
      where
        (digit, carry') = addDigits carry a b
        rest = go carry' as bs

-- | Sums list in forward order by first padding
-- the smaller list with zeroes, then "looking ahead"
-- with recursion.
sumListsTwo :: [Int] -> [Int] -> [Int]
sumListsTwo a b = addCarry $ uncurry go $ padLists a b
  where
    go [] []         = ([], 0)
    go es []         = go es [0]
    go [] es         = go [0] es
    go (a:as) (b:bs) = (digit:rest, carry')
      where
        (rest, carry) = go as bs
        (digit, carry') = addDigits carry a b

    addCarry (l, 0)     = l
    addCarry (l, carry) = carry:l

padLists :: [Int] -> [Int] -> ([Int], [Int])
padLists l1 l2
    | length l1 > length l2 = (l1, pad l2 (length l1))
    | length l1 < length l2 = (pad l1 (length l2), l2)
    | otherwise             = (l1, l2)
  where
    pad l len = replicate (len - length l) 0 ++ l

addDigits :: Int -> Int -> Int -> (Int, Int)
addDigits carry a b = (digit, carry')
  where
    added = carry + a + b
    digit = added `rem` 10
    carry' = if added >= 10 then 1 else 0
