module CTCI.Arrays.PalindromePermutation
    ( palindromePermutation
    , palindromePermutationTwo
    ) where

import Data.Bits
import Data.Char
import Data.List
import Data.Maybe

import qualified Data.IntMap.Strict as IM

-- Given a string, write a function to check if it is a permutation
-- of a palindrome. A palindrome is a word or phrase that is the same
-- forwards and backwards. A permutation is a rearrangement of letters.
-- The palindrome does not need to be limited to just dictionary words.

-- Example:
-- Input: "Tact Coa"
-- Output: True (permutations: "taco cat", "atco cta", etc.)

-- | First solution is to walk through the string and toggle the bit for
-- each character in a bitstring, then check if the resulting bitstring
-- only has at most one 1 bit.
palindromePermutation :: String -> Bool
palindromePermutation s = onlyOneBit bits
  where
    toggle bits c
        | bits .&. mask == 0 = bits .|. mask
        | otherwise          = bits .&. complement mask
      where
        mask = shiftL 1 c

    bits = foldl' toggle 0 . catMaybes . fmap charNumber $ s :: Int

    onlyOneBit bits = bits == 0 || bits .&. (bits - 1) == 0

-- | Second solution is to build a hashtable to store character -> count,
-- then walk through the hashtable and check that at most one key value pair
-- has an odd count.
palindromePermutationTwo :: String -> Bool
palindromePermutationTwo =
    isPalindrome . buildHashTable . catMaybes . fmap charNumber
  where
    buildHashTable = foldl' incrOrInsert IM.empty
    incrOrInsert map c
        | IM.member c map = IM.adjust (+ 1) c map
        | otherwise       = IM.insert c 1 map
    isPalindrome = (<= 1) . length . filter odd . IM.elems

charNumber :: Char -> Maybe Int
charNumber c
    | isLetter c = Just (ord (toLower c) - ord 'a')
    | otherwise  = Nothing
