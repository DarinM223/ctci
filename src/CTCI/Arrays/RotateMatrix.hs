{-# LANGUAGE LambdaCase #-}
module CTCI.Arrays.RotateMatrix where

import Control.Monad
import Data.Array.MArray

-- Given an image represented by an NxN matrix, where each pixel
-- in the image is 4 bytes, write a method to rotate the image
-- by 90 degrees. Can you do this in place?

-- How to test example:
-- > import Data.Array.IO
-- > m <- listToMatrix 3 [1, 2, 3, 4, 5, 6, 7, 8, 9] :: IO (IOArray (Int, Int) Int)
-- > rotateMatrix m
-- > matrixToList m
-- [7,4,1,8,5,2,9,6,3]

-- | Walk through each layer in the matrix and
-- do a four way swap for each index in the edge so
-- that all of the edges are swapped.
rotateMatrix :: (MArray a e m) => a (Int, Int) e -> m Bool
rotateMatrix matrix = getBounds matrix >>= \case
    (_, (endRow, endCol)) | endRow >= 0, endRow == endCol -> do
        let n = endRow + 1
        mapM_ (rotateLayer n matrix) [0..n `quot` 2 - 1]
        return True
    _ -> return False
  where
    fourWaySwap matrix top left bot right = do
        tempTop <- readArray matrix top

        left' <- readArray matrix left
        writeArray matrix top left'

        bot' <- readArray matrix bot
        writeArray matrix left bot'

        right' <- readArray matrix right
        writeArray matrix bot right'

        writeArray matrix right tempTop

    rotateLayer n matrix layer =
        forM_ [first..last - 1] $ \i -> do
            let offset = i - first
                top    = (first, i)
                left   = (last - offset, first)
                bot    = (last, last - offset)
                right  = (i, last)
            fourWaySwap matrix top left bot right
      where
        first = layer
        last = n - 1 - layer

listToMatrix :: (MArray a e m) => Int -> [e] -> m (a (Int, Int) e)
listToMatrix n = newListArray ((0, 0), (n - 1, n - 1))

matrixToList :: (MArray a e m) => a (Int, Int) e -> m [e]
matrixToList = getElems
