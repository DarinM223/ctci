module CTCI.Arrays.CheckPermutation where

import Data.List

import qualified Data.HashMap.Strict as H

-- Given two strings, write a method to decide if one is a permutation of the other.

-- | First solution is to build a hashtable from character to number
-- of occurences for the first string, then go through the second string
-- and decrement the hashtable for each character. Then the strings aren't
-- permutations of each other if a key-value pair in the hashtable isn't 0.
-- The time complexity is O(n).
checkPermutation :: String -> String -> Bool
checkPermutation s1 s2 = checkPairs . decrMap s2 . buildMap $ s1
  where
    buildMap = foldl' (flip $ applyOrInsert (+ 1) 1) H.empty
    decrMap s map = foldl' (flip $ applyOrInsert (subtract 1) (-1)) map s
    checkPairs = null . filter (/= 0) . H.elems

    applyOrInsert f defaultValue c map
        | H.member c map = H.adjust f c map
        | otherwise      = H.insert c defaultValue map

-- | Second solution is to sort both strings and then compare them.
-- The time complexity is O(n * log(n))
checkPermutationTwo :: String -> String -> Bool
checkPermutationTwo s1 s2 = sort s1 == sort s2
