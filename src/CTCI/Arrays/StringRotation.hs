module CTCI.Arrays.StringRotation (isRotation) where

import Data.List

-- Assume you have a method isSubstring which checks if one word is
-- a substring of another. Given two strings s1 and s2, write code
-- to check if s2 is a rotation of s1 using only one call to isSubstring
-- (e.g., "waterbottle" is a rotation of "erbottlewat").

isSubstring = flip isInfixOf

isRotation :: String -> String -> Bool
isRotation s1 s2
    | length s1 == length s2 && not (null s1) = isSubstring (s1 ++ s1) s2
    | otherwise                               = False
