{-# LANGUAGE FlexibleContexts #-}
module CTCI.Arrays.URLify where

import Data.Array.MArray

-- Write a method to replace all spaces in a string with '%20'.
-- You may assume that the string has sufficient space at the end to
-- hold the additional characters, and that you are given the "true"
-- length of the string. (Note: if implementing in Java, please use
-- a character array so that you can perform this operation in place.)

-- EXAMPLE
-- Input: "Mr John Smith    ", 13
-- Output: "Mr%20John%20Smith"

-- How to test example:
-- > import Data.Array.IO
-- > name <- johnsmith :: IO (IOArray Int Char)
-- > arrToStr name
-- "Mr John Smith    "
-- > urlify 13 name
-- > arrToStr name
-- "Mr%20John%20Smith"

-- | Solution is to walk backward from the "true" length
-- while also keeping track of the end of the string.
-- For each character, if it is a space write "%20" to the
-- end of the string, otherwise write the character to the
-- end of the string.
urlify :: (MArray a Char m) => Int -> a Int Char -> m ()
urlify len arr = do
    numSpaces <- countSpaces 0 [0..len-1] arr
    let end = len - 1 + numSpaces * 2
    go (len - 1) end arr
  where
    countSpaces acc [] _ = return acc
    countSpaces acc (i:is) arr = do
        ch <- readArray arr i
        case ch of
            ' ' -> countSpaces (acc + 1) is arr 
            _   -> countSpaces acc is arr

    go trueEnd end arr
        | trueEnd < 0 = return ()
        | otherwise = do
            ch <- readArray arr trueEnd
            case ch of
                ' ' -> do
                    writeArray arr end '0'
                    writeArray arr (end - 1) '2'
                    writeArray arr (end - 2) '%'
                    go (trueEnd - 1) (end - 3) arr
                _ -> do
                    writeArray arr end ch
                    go (trueEnd - 1) (end - 1) arr

strToArr :: (MArray a Char m) => String -> m (a Int Char)
strToArr s = newListArray (0, length s - 1) s

arrToStr :: (MArray a Char m) => a Int Char -> m String
arrToStr = getElems

johnsmith :: (MArray a Char m) => m (a Int Char)
johnsmith = strToArr "Mr John Smith    "
