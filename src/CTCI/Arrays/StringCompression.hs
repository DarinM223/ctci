module CTCI.Arrays.StringCompression
    ( compress
    , compressTwo
    ) where

import Data.Foldable
import Data.Sequence ((><), (|>))

import qualified Data.Sequence as S

-- Implement a method to perform basic string compression using the counts
-- of repeated characters. For example, the string aabcccccaaa would become
-- a2b1c5a3. If the "compressed" string would not become smaller than the
-- original string, your method should return the original string.
-- You can assume the string has only uppercase and lowercase letters (a-z).

-- | First solution is to check the length of the compressed
-- string before compressing the string.
compress :: String -> String
compress s
    | compressedLength < length s = compressString s
    | otherwise                   = s
  where
    inc _ count n = n + 1 + length (show count)
    compressedLength = compressFold inc 0 s

-- | Second solution is to compress the string
-- and throw it away if it is bigger than the original.
compressTwo :: String -> String
compressTwo s
    | length compressed < length s = compressed
    | otherwise                    = s
  where
    compressed = compressString s

compressString :: String -> String
compressString = toList . compressFold append S.empty
  where
    append ch count s = (s |> ch) >< S.fromList (show count)

compressFold :: (Char -> Int -> a -> a) -> a -> String -> a
compressFold = go Nothing
  where
    go Nothing f acc (c:cs)        = go (Just (c, 1)) f acc cs
    go Nothing _ acc []            = acc
    go (Just (ch, count)) f acc [] = f ch count acc
    go (Just (ch, count)) f acc (c:cs)
        | ch /= c   = go (Just (c, 1)) f (f ch count acc) cs
        | otherwise = go (Just (ch, count + 1)) f acc cs
