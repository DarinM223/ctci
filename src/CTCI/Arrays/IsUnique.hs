module CTCI.Arrays.IsUnique
    ( isUnique
    , isUniqueTwo
    ) where

import Data.Bits
import Data.Char
import Data.List

-- Implement an algorithm to determine if a string has all unique characters.
-- What if you cannot use additional data structures?

-- | First solution is to set a bit vector
-- for every character. If the bit vector
-- was already set, the string is not unique.
--
-- The time complexity is O(n).
isUnique :: String -> Bool
isUnique = go 0
  where
    go _ [] = True
    go bits (c:cs)
        | contains c' bits = False
        | otherwise        = go (add c' bits) cs
      where
        c' = ord c - ord 'a'

contains :: Int -> Int -> Bool
contains c bits = (bits .&. shiftL 1 c) /= 0

add :: Int -> Int -> Int
add c bits = bits .|. shiftL 1 c

-- | Second solution is to sort the string
-- and then walk through the string checking for
-- duplicate values.
--
-- The time complexity is O(n * log(n)).
isUniqueTwo :: String -> Bool
isUniqueTwo = checkPairs . sort
  where
    checkPairs []  = True
    checkPairs [_] = True
    checkPairs (a:b:xs)
        | a == b    = False
        | otherwise = checkPairs (b:xs)
