{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
module CTCI.Arrays.ZeroMatrix where

import Control.Monad
import Data.Array.MArray

-- Write an algorithm such that if an element in an MxN matrix
-- is 0, its entire row and column are set to 0.

-- How to test example:
-- > import Data.Array.IO
-- > m <- listToMatrix 3 [1, 2, 0, 0, 5, 6, 7, 8, 9] :: IO (IOArray (Int, Int) Int)
-- > zeroMatrix m
-- > matrixToList m
-- [0,0,0,0,0,0,0,8,0]

-- | Stores if the row or column is to be zeroed out in the
-- first row and the first column of the matrix. Since the
-- first row or the first column may have already had a zero in it,
-- use two booleans to remember if the first row or the first column
-- needs to be zeroed out also.
zeroMatrix :: (MArray a Int m) => a (Int, Int) Int -> m ()
zeroMatrix matrix = getBounds matrix >>= \case
    (_, (endRow, endCol)) | endRow >= 0, endCol >= 0 -> do
        firstRowHasZero <- checkFirstRow matrix
        firstColHasZero <- checkFirstCol matrix
        markZeroRowsAndCols matrix
        applyMarks firstRowHasZero firstColHasZero matrix
    _ -> return ()
  where
    checkZeros _ [] = return False
    checkZeros matrix (p:ps) = do
        v <- readArray matrix p
        if v == 0
            then return True
            else checkZeros matrix ps

    checkFirstRow matrix = do
        (_, (_, endCol)) <- getBounds matrix
        checkZeros matrix . fmap ((,) 0) $ [0..endCol]

    checkFirstCol matrix = do
        (_, (endRow, _)) <- getBounds matrix
        checkZeros matrix . fmap (flip (,) 0) $ [0..endRow]

    markZeroRowsAndCols matrix = do
        (_, (endRow, endCol)) <- getBounds matrix
        forM_ [(r, c) | r <- [1..endRow], c <- [1..endCol]] $ \(r, c) -> do
            v <- readArray matrix (r, c)
            when (v == 0) $ do
                writeArray matrix (0, c) 0
                writeArray matrix (r, 0) 0

    setRow matrix row endCol =
        mapM_ (\c -> writeArray matrix (row, c) 0) [0..endCol]
    setCol matrix col endRow =
        mapM_ (\r -> writeArray matrix (r, col) 0) [0..endRow]

    applyMarks row1mark col1mark matrix = do
        (_, (endRow, endCol)) <- getBounds matrix

        forM_ [1..endCol] $ \c -> do
            v <- readArray matrix (0, c)
            when (v == 0) $ setCol matrix c endRow

        forM_ [1..endRow] $ \r -> do
            v <- readArray matrix (r, 0)
            when (v == 0) $ setRow matrix r endCol

        when row1mark $ setRow matrix 0 endCol
        when col1mark $ setCol matrix 0 endRow
