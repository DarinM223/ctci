module CTCI.Arrays.OneAway where

-- There are three types of edits that can be performed
-- on strings: insert a character, remove a character,
-- or replace a character. Given two strings, write a
-- function to check if they are one edit (or zero edits)
-- away.

-- Example:
-- pale,  ple  -> true
-- pales, pale -> true
-- pale,  bale -> true
-- pale,  bake -> false


-- | Because there can be only one edit, the remove, insert, and modify
-- checks can be separated. All of the checks involve walking through
-- both strings, and if two characters are different a flag is set to True
-- and any subsequent differences fail.
--
-- For remove and insert, if two characters are different,
-- the longer string skips their character. For modify, both strings skip
-- their characters if they are different.
--
-- Note: Verify that the lengths are one difference away
-- upfront to improve performance and simplify the algorithm.
oneAway :: String -> String -> Bool
oneAway s1 s2
    | length s1 == length s2 + 1 = check handleRemove False s1 s2
    | length s2 == length s1 + 1 = check handleInsert False s1 s2
    | length s1 == length s2     = check handleModify False s1 s2
    | otherwise                  = False
  where
    handleRemove (_:as) s2 = (as, s2)
    handleRemove _ _       = undefined

    handleInsert s1 (_:bs) = (s1, bs)
    handleInsert _ _       = undefined

    handleModify (_:as) (_:bs) = (as, bs)
    handleModify _ _       = undefined

    check _ _ [] []       = True
    check _ flag (_:_) [] = not flag
    check _ flag [] (_:_) = not flag
    check _ True (a:_) (b:_)
        | a /= b = False
    check handle flag s1@(a:as) s2@(b:bs)
        | a /= b =
            let (s1', s2') = handle s1 s2
            in check handle True s1' s2'
        | otherwise = check handle flag as bs
