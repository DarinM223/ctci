{-# LANGUAGE RecursiveDo #-}

module CTCI.TreesGraphs.Successor where

-- Write an algorithm to find the "next" node (i.e. in-order successor) of
-- a given node in a binary search tree. You may assume that each node has
-- a link to its parent.

import Control.Applicative ((<|>))
import Control.Monad (join)
import Data.IORef

data Node a = Node
    { _data   :: a
    , _left   :: Maybe (IORef (Node a))
    , _right  :: Maybe (IORef (Node a))
    , _parent :: Maybe (IORef (Node a))
    }

-- | The successor can be in:
-- 1. The leftmost child of the node's right child
-- 2. The first parent that contains the node as a
-- child in the parent's left subtree.
--
-- The code checks the first case first and checks the second case if the
-- first case fails.
successor :: Maybe (IORef (Node a)) -> IO (Maybe (IORef (Node a)))
successor node = do
    rightMaybe <- join <$> traverse lookRight node
    parentMaybe <- join <$> traverse lookParent node
    return $ rightMaybe <|> parentMaybe
  where
    lookRight :: IORef (Node a) -> IO (Maybe (IORef (Node a)))
    lookRight nodeRef = readIORef nodeRef >>= traverse lookLeft . _right

    lookLeft :: IORef (Node a) -> IO (IORef (Node a))
    lookLeft nodeRef = do
        node <- readIORef nodeRef
        case _left node of
            Just left -> lookLeft left
            Nothing   -> return nodeRef

    lookParent :: IORef (Node a) -> IO (Maybe (IORef (Node a)))
    lookParent nodeRef = do
        node <- readIORef nodeRef
        case _parent node of
            Just parentRef -> do
                parent <- readIORef parentRef
                if _left parent == Just (nodeRef)
                    then return $ Just parentRef
                    else lookParent parentRef
            Nothing -> return Nothing

printTreeNode :: (Show a) => Maybe (IORef (Node a)) -> IO ()
printTreeNode Nothing        = putStrLn "Nothing"
printTreeNode (Just nodeRef) = readIORef nodeRef >>= print . _data

-- | Successor should be node with value 4.
--
--             6
--            / \
--          [3]  7
--          / \
--         2   5
--            /
--           4
testSuccessor :: IO (Maybe (IORef (Node Int)))
testSuccessor = mdo
    six <- newIORef $ Node 4 (Just three) (Just seven) Nothing
    three <- newIORef $ Node 3 (Just two) (Just five) (Just six)
    seven <- newIORef $ Node 7 Nothing Nothing (Just six)
    two <- newIORef $ Node 2 Nothing Nothing (Just three)
    five <- newIORef $ Node 5 (Just four) Nothing (Just three)
    four <- newIORef $ Node 4 Nothing Nothing (Just five)
    return $ Just three

-- | Successor should be node with value 8.
--
--               8
--              / \
--             3   9
--            / \
--           2   5
--              / \
--             4  [7]
--                /
--               6
testSuccessor' :: IO (Maybe (IORef (Node Int)))
testSuccessor' = mdo
    eight <- newIORef $ Node 8 (Just three) (Just nine) Nothing
    three <- newIORef $ Node 3 (Just two) (Just five) (Just eight)
    nine <- newIORef $ Node 9 Nothing Nothing (Just eight)
    two <- newIORef $ Node 2 Nothing Nothing (Just three)
    five <- newIORef $ Node 5 (Just four) (Just seven) (Just three)
    four <- newIORef $ Node 4 Nothing Nothing (Just five)
    seven <- newIORef $ Node 7 (Just six) Nothing (Just five)
    six <- newIORef $ Node 6 Nothing Nothing (Just seven)
    return $ Just seven

-- | Successor should be Nothing.
testSuccessor'' :: IO (Maybe (IORef (Node Int)))
testSuccessor'' = fmap Just $ newIORef $ Node 2 Nothing Nothing Nothing
