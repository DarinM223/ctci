module CTCI.TreesGraphs.CheckBalanced where

-- Implement a function to check if a binary tree is balanced. For the
-- purposes of this question, a balanced tree is defined to be a tree such
-- that the heights of the two subtrees of any node never differ by more
-- than one.

import Control.Monad (guard)
import Data.Maybe (isJust)

data Tree = Nil | Node Tree Tree
    deriving (Show)

checkBalanced :: Tree -> Bool
checkBalanced = isJust . go
  where
    go Nil               = Just 0
    go (Node left right) = do
        lh <- go left
        rh <- go right
        guard (abs (lh - rh) <= 1)
        return $ max lh rh + 1

balancedTree :: Tree
balancedTree = one
  where
    one = Node three two
    two = Node four five
    three = Node six Nil
    four = Node Nil Nil
    five = Node Nil seven
    six = Node Nil Nil
    seven = Node Nil Nil

unbalancedTree :: Tree
unbalancedTree = one
  where
    one = Node three two
    two = Node seven four
    three = Node Nil five
    four = Node Nil Nil
    five = Node six Nil
    six = Node Nil Nil
    seven = Node Nil Nil
