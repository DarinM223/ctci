{-# LANGUAGE RecursiveDo #-}

module CTCI.TreesGraphs.RouteBetweenNodes where

-- Given a directed graph, design an algorithm to find out whether there is
-- a route between two nodes.

import Data.Foldable (foldl')
import Data.IORef
import Data.Sequence

data GraphNode = GraphNode
    { _edges   :: [IORef GraphNode]
    , _visited :: Bool
    }

-- | BFS traversal starting at the start node looking for the end node.
--
-- Marks nodes as visited to avoid infinite cycles.
routeBetweenNodes :: IORef GraphNode -> IORef GraphNode -> IO Bool
routeBetweenNodes start end = go end (Empty :|> start)
  where
    go _ Empty = return False
    go end (node :<| rest)
        | node == end = return True
        | otherwise   = do
            node' <- readIORef node
            if _visited node'
                then go end rest
                else do
                    writeIORef node node' { _visited = True }
                    go end $ foldl' (:|>) rest $ _edges node'

-- | Creates a graph looking like this and returns nodes 1 and 5.
--
--      4
--     /
--    2
--   / \\
-- [1]   3
--        \
--        [5]
--
testGraph :: IO (IORef GraphNode, IORef GraphNode)
testGraph = mdo
    one <- newIORef GraphNode { _edges = [two], _visited = False }
    two <- newIORef GraphNode { _edges = [three, four], _visited = False }
    three <- newIORef GraphNode { _edges = [two, five], _visited = False }
    four <- newIORef GraphNode { _edges = [], _visited = False }
    five <- newIORef GraphNode { _edges = [], _visited = False }
    return (one, five)

-- | Creates a graph looking like this and returns nodes 4 and 5.
--
--      <--
--    -2-->[4]
--   / ^
--  /  |
-- 1-->3-->[5]
--
testGraph' :: IO (IORef GraphNode, IORef GraphNode)
testGraph' = mdo
    _ <- newIORef GraphNode { _edges = [two, three], _visited = False }
    two <- newIORef GraphNode { _edges = [four], _visited = False }
    three <- newIORef GraphNode { _edges = [two, five], _visited = False }
    four <- newIORef GraphNode { _edges = [two], _visited = False }
    five <- newIORef GraphNode { _edges = [], _visited = False }
    return (four, five)
