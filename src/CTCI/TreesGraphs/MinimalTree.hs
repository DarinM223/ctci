module CTCI.TreesGraphs.MinimalTree where

import Data.Array

-- Given a sorted (increasing order) array with unique integer elements,
-- write an algorithm to create a binary search tree with minimal height.

data Tree a = Nil | Node a (Tree a) (Tree a)
    deriving (Show)

minimalTree :: Array Int a -> Tree a
minimalTree a = go (bounds a) a
  where
    go (start, end) arr
        | end < start = Nil
        | otherwise   = Node midElem (go (start, mid - 1) arr)
                                     (go (mid + 1, end) arr)
      where
        mid = (start + end) `div` 2
        midElem = arr ! mid
