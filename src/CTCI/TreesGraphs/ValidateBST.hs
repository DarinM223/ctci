module CTCI.TreesGraphs.ValidateBST where

-- Implement a function to check if a binary tree is a binary search tree.

import Control.Monad (guard)
import Data.Maybe (isJust)

data Tree a = Nil | Node a (Tree a) (Tree a)
    deriving (Show)

-- | Does an in-order traversal and saves the last element traversed.
-- If the every current element is greater than the previous element, the
-- BST is valid.
validateBST :: (Ord a) => Tree a -> Bool
validateBST = isJust . go Nothing
  where
    go :: (Ord a) => Maybe a -> Tree a -> Maybe ()
    go _ Nil                    = Just ()
    go prev (Node v left right) = do
        go prev left
        guard (maybe True (v >) prev)
        go (Just v) right

-- | Min-max solution that propogates the range of valid values down the
-- tree.
validateBST' :: (Ord a) => Tree a -> Bool
validateBST' = go Nothing Nothing
  where
    inRange :: (Ord a) => Maybe a -> Maybe a -> a -> Bool
    inRange Nothing Nothing _           = True
    inRange Nothing (Just max) a        = a <= max
    inRange (Just min) Nothing a        = a > min
    inRange min@(Just _) max@(Just _) a =
        inRange Nothing max a && inRange min Nothing a

    go :: (Ord a) => Maybe a -> Maybe a -> Tree a -> Bool
    go _ _ Nil = True
    go min max (Node v left right)
        | inRange min max v = go min (Just v) left && go (Just v) max right
        | otherwise         = False

-- | Expect true
--
--     2
--    / \
--   2   3
--        \
--         4
testBST :: Tree Int
testBST = two
  where
    two = Node 2 two' three
    two' = Node 2 Nil Nil
    three = Node 3 Nil four
    four = Node 4 Nil Nil

-- | Expect false
--
--     2
--    / \
--   1   2
--        \
--         4
testBST' :: Tree Int
testBST' = two
  where
    two = Node 2 one two'
    one = Node 1 Nil Nil
    two' = Node 2 Nil four
    four = Node 4 Nil Nil

-- | Expect false
--
--      4
--     / \
--    /   \
--   2     6
--  / \   / \
-- 1   3 4   7
testBST'' :: Tree Int
testBST'' = four
  where
    four = Node 4 two six
    two = Node 2 one three
    six = Node 6 four' seven
    one = Node 1 Nil Nil
    three = Node 3 Nil Nil
    four' = Node 4 Nil Nil
    seven = Node 7 Nil Nil
