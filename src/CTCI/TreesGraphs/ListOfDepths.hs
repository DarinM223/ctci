module CTCI.TreesGraphs.ListOfDepths where

-- Given a binary tree, design an algorithm which creates a linked list of
-- all the nodes at each depth (e.g., if you have a tree with depth D,
-- you'll have D linked lists).

import qualified Data.IntMap as IM

data Tree a = Nil | Node a (Tree a) (Tree a)
    deriving (Show)

listOfDepths :: Tree a -> IM.IntMap [a]
listOfDepths Nil  = IM.empty
listOfDepths tree = go IM.empty 0 tree
  where
    insertOrAdjust f k v map
        | IM.member k map = IM.adjust f k map
        | otherwise       = IM.insert k v map
    go map _ Nil                 = map
    go map i (Node v left right) = map'''
      where
        map' = insertOrAdjust (v:) i [v] map
        map'' = go map' (i + 1) left
        map''' = go map'' (i + 1) right

testTree :: Tree Int
testTree = one
  where
    one = Node 1 two Nil
    two = Node 2 four three
    three = Node 3 Nil five
    four = Node 4 six Nil
    five = Node 5 Nil Nil
    six = Node 6 seven Nil
    seven = Node 7 Nil Nil
