{-# LANGUAGE TupleSections #-}
module CTCI.TreesGraphs.BuildOrder where

import Data.Char
import Data.Foldable
import Data.Maybe (listToMaybe)
import qualified Data.IntMap.Strict as IM

-- You are given a list of projects and a list of dependencies (which is
-- a list of pairs of projects, where the second project is dependent on
-- the first project). All of a project's dependencies must be built before
-- the project is. Find a build order that will allow the projects to be
-- built. If there is no valid build order, return an error.
--
-- Example:
-- Input:
--   projects: a, b, c, d, e, f
--   dependencies: (a, d), (f, b), (b, d), (f, a), (d, c)
-- Output: f, e, a, b, d, c

data GraphNode = GraphNode
    { _edges    :: [Char]
    , _incoming :: Int
    }

type Graph = IM.IntMap GraphNode

mkGraph :: [Char] -> [(Char, Char)] -> Graph
mkGraph projs deps = foldl' addDep nodes deps
  where
    nodes = IM.fromList $ fmap ((, GraphNode [] 0) . ord) projs
    addDep nodes (start, end)
        = IM.adjust (\n -> n { _incoming = _incoming n + 1 }) (ord end)
        . IM.adjust (\n -> n { _edges = end:_edges n }) (ord start)
        $ nodes

calcBuildOrder :: Graph -> [Char]
calcBuildOrder graph = case noIncoming of
    Just (i, _) -> chr i:calcBuildOrder (remove i graph)
    Nothing     -> []
  where
    noIncoming = listToMaybe
               . take 1
               . filter ((== 0) . _incoming . snd)
               $ IM.toList graph
    remove i graph = case IM.lookup i graph of
        Just node -> IM.delete i $ foldl' decNode graph (_edges node)
        Nothing   -> graph
      where
        decIncr node = node { _incoming = _incoming node - 1 }
        decNode graph ch = IM.adjust decIncr (ord ch) graph

buildOrder :: [Char] -> [(Char, Char)] -> [Char]
buildOrder projects dependencies = calcBuildOrder
                                 $ mkGraph projects dependencies
