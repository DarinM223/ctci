module CTCI.DynamicProgramming.RecursiveMultiply where

-- Write a recursive function to multiply two positive integers without
-- using the * operator. You can use addition, subtraction, and bit
-- shifting, but you should minimize the number of those operations.

import Data.Bits

-- | 7 * 8 = 8 + 8 + 8 + 8 + 8 + 8 + 8
--
-- Just adding these numbers is O(n) where n is the smaller of the two.
--
-- Next smallest time complexity is O(log n)
--
-- In order to do this, we have to make sure that we are cutting the space
-- in two every time.
--
-- If the smaller is even, we can multiply (smaller / 2) by the bigger and
-- then double that.
--
-- If smaller is odd, you do the same as if it was even, but add an extra
-- row at the end. So (smaller / 2) * bigger * 2 + bigger
--
-- (x / 2) == (x >> 1), (x * 2) == (x + x), and multiplying by bigger can
-- be done with recursive calls, so everything can be done with the allowed
-- operators.
--
-- This has O(log n) time where n is the smaller of the two numbers.
recursiveMultiply :: Int -> Int -> Int
recursiveMultiply a b = multiply smaller bigger
  where
    (smaller, bigger) | a < b     = (a, b)
                      | otherwise = (b, a)
    multiply 0 _      = 0
    multiply 1 bigger = bigger
    multiply smaller bigger
        | smaller `rem` 2 == 0 = halfResult + halfResult
        | otherwise            = halfResult + halfResult + bigger
      where
        half = shiftR smaller 1
        halfResult = multiply half bigger
