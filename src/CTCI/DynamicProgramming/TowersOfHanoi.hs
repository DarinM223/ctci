{-# LANGUAGE BangPatterns #-}
module CTCI.DynamicProgramming.TowersOfHanoi where

-- In the classic problem of the Towers of Hanoi, you have 3 towers and
-- N disks of different sizes which can slide onto any tower. The puzzle
-- starts with disks sorted in ascending order of size from top to
-- bottom (i.e. each disk sits on top of an even larger one). You have
-- the following constraints:
--
-- (1) Only one disk can be moved at a time
-- (2) A disk is slid off the top of one tower onto another tower.
-- (3) A disk cannot be placed on  top of a smaller disk.
--
-- Write a program to move the disks from the first tower to the last using
-- stacks.

hanoi :: Int   -- ^ The number of elements to move from source to destination
      -> [Int] -- ^ The source tower
      -> [Int] -- ^ The temporary tower
      -> [Int] -- ^ The destination tower
      -> ([Int], [Int], [Int])
hanoi 0 t1 t2 t3     = (t1, t2, t3)
hanoi 1 (e:t1) t2 t3 = (t1, t2, e:t3)
hanoi 1 [] _ _       = error "Cannot move from empty tower"
hanoi n !t1 !t2 !t3  = (t1''', t2''', t3''')
  where
    (t1', t3', t2')       = hanoi (n - 1) t1 t3 t2
    (t1'', t2'', t3'')    = hanoi 1 t1' t2' t3'
    (t2''', t1''', t3''') = hanoi (n - 1) t2'' t1'' t3''
