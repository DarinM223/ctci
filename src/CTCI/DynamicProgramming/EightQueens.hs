{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TupleSections #-}
module CTCI.DynamicProgramming.EightQueens where

-- Write an algorithm to print all ways of arranging eight queens on a 8x8
-- chess board so that none of them share the same row, column, or
-- diagonal. In this case, "diagonal" means all diagonals, not just the two
-- that bisect the board.

import Data.Foldable (foldl')

-- | For every column, find choices on which row to place a queen at, and
-- recursively add the results of each choice (similar to the permutations
-- questions).
eightQueens :: [[(Int, Int)]]
eightQueens = go [] [] 0
  where
    go build queens i | i > 7 = queens:build
    go build !queens i = foldl' foldChoice build choices
      where
        noConflict [] _ = True
        noConflict ((r', c'):qs) (r, c)
            | r == r'                = False
            | abs (r - r') == c - c' = False
            | otherwise              = noConflict qs (r, c)
        choices = filter (noConflict queens) $ (, i) <$> [0..7]
        foldChoice build p = go build (p:queens) (i + 1)
