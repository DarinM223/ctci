module CTCI.DynamicProgramming.MagicIndex where

import Data.Array
import Data.Maybe

-- A magic index in an array A[0..n-1] is defined to be an index such that
-- A[i] = i. Given a sorted array of distinct integers, write a method to
-- find a magic index, if one exists, in array A.
--
-- Follow up: What if the values are not distinct?

-- Example:
-- i:    [ 0,      1,   2,   3, 4, 5, .. ]
-- a[i]: [ -100, -90, -89, -82, 4, 6, .. ]
--
-- Get middle of array
-- if i == a[i] return
-- if i > a[i] search right
-- if i < a[i] search left
--
-- O(log N)
magicIndex :: Array Int Int -> Maybe Int
magicIndex arr = go (bounds arr) arr
  where
    go (start, end) arr | end < start = Nothing
                        | i > arr ! i = go (i + 1, end) arr   -- search right
                        | i < arr ! i = go (start, i - 1) arr -- search left
                        | otherwise   = Just i
      where
        i = (start + end) `quot` 2

-- | For not distinct values you have to search both sides but you might be
-- able to exclude values right after or before the midpoint.
magicIndexNotDistinct :: Array Int Int -> Maybe Int
magicIndexNotDistinct arr = go (bounds arr) arr
  where
    go (start, end) arr | end < start = Nothing
                        | i == mid    = Just i
                        | isJust left = left
                        | otherwise   = right
      where
        i = (start + end) `quot` 2
        mid = arr ! i
        right = go (max (i + 1) mid, end) arr
        left = go (start, min (i - 1) mid) arr

sampleMagicIndex :: Array Int Int
sampleMagicIndex = listArray (0, 10) [-100, -90, -89, -82, 4, 6, 7, 8, 9, 10]

sampleMagicIndexNotDistinct :: Array Int Int
sampleMagicIndexNotDistinct = listArray (0, 10)
    [4, 4, 5, 5, 5, 5, 6, 6, 6, 6]
