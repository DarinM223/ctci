module CTCI.DynamicProgramming.RobotInAGrid where

import Data.Array

-- Imagine a robot sitting on the upper left corner of grid with r rows and
-- c columns. The robot can only move in two directions, right and down,
-- but certain cells are "off limits" such that the robot cannot step on
-- them. Design an algorithm to find a path for the robot from the top left
-- to the bottom right.

data Move = MoveRight | MoveDown deriving (Show, Eq)

-- | Naive way that uses DFS.
robotPathNaive :: Array (Int, Int) Bool -> [Move]
robotPathNaive = snd . go (0, 0)
  where
    go (r, c) arr
        | r == endRow, c == endCol = (True, [])
        | r >= 0, r <= endRow, c >= 0, c <= endCol, arr ! (r, c) =
            case (resRight, resDown) of
                ((True, moves), _) -> (True, MoveRight:moves)
                (_, (True, moves)) -> (True, MoveDown:moves)
                _                  -> (False, [])
        | otherwise = (False, [])
      where
        (_, (endRow, endCol)) = bounds arr
        resDown = go (r + 1, c) arr
        resRight = go (r, c + 1) arr

-- | Builds a table of elements where if an element in the table
-- is true, then it can be added to the result path. Then, starting at the
-- bottom right, it checks the left and up positions in the table and
-- chooses one that succeeds to the path, and moves to that position.
--
-- This can only be done because the robot can't move backwards, which
-- means you can use at the previous results behind you to calculate the
-- current position.
robotPath :: Array (Int, Int) Bool -> [Move]
robotPath arr = go (endRow, endCol) []
  where
    arrBounds@(_, (endRow, endCol)) = bounds arr

    go (0, 0) moves = moves
    go (r, c) moves
        | get left  = go left (MoveRight:moves)
        | get up    = go up (MoveDown:moves)
        | otherwise = []
      where
        left = (r, c - 1)
        up = (r - 1, c)

    build (0, 0) = True
    build (r, c) = arr ! (r, c) && (get (r - 1, c) || get (r, c - 1))

    get (r, c) | r >= 0, c >= 0 = inPathArr ! (r, c)
               | otherwise      = False

    inPathArr = listArray arrBounds [build p | p <- range arrBounds]

-- _ X _ _ _
-- _ _ _ _ _
-- _ X _ _ _
-- X _ _ X X
-- _ _ _ _ _
samplePath :: Array (Int, Int) Bool
samplePath = listArray ((0, 0), (4, 4))
    [ True, False, True, True, True
    , True, True, True, True, True
    , True, False, True, True, True
    , False, True, True, False, False
    , True, True, True, True, True
    ]

-- _ X _ _ _
-- _ X _ _ _
-- _ _ X _ _
-- _ X _ _ _
-- X _ _ _ _
blocked :: Array (Int, Int) Bool
blocked = listArray ((0, 0), (4, 4))
    [ True, False, True, True, True
    , True, False, True, True, True
    , True, True, False, True, True
    , True, False, True, True, True
    , False, True, True, True, True
    ]

allOpen :: Array (Int, Int) Bool
allOpen = listArray ((0, 0), (4, 4))
    [ True, True, True, True, True
    , True, True, True, True, True
    , True, True, True, True, True
    , True, True, True, True, True
    , True, True, True, True, True
    ]
