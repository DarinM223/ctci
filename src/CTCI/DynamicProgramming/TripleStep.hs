module CTCI.DynamicProgramming.TripleStep where

import Data.Array
import qualified Data.Array as A

-- A child is running up a staircase with n steps and can hop either
-- 1 step, 2 steps, or 3 steps at a time. Implement a method to count how
-- many possible ways a child can run up the stairs.

tripleStep :: Int -> Int
tripleStep n = (arr ! n)
  where
    go 0 = 1
    go n = get (n - 1) + get (n - 2) + get (n - 3)
      where
        get n | n < 0     = 0
              | otherwise = arr ! n
    arr = A.listArray (0, n) [go i | i <- A.range (0, n)]
