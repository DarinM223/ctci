{-# LANGUAGE BangPatterns #-}
module CTCI.DynamicProgramming.PermutationsWithDups where

-- Write a method to compute all permutations of a string whose characters
-- are not necessarily unique. The list of permutations should not have
-- duplicates.

import Data.Foldable (foldl')
import qualified Data.HashMap.Strict as H

-- | Builds strings one character at a time, similar to permutations'' in
-- the permutations without duplicates problem. The main difference is how
-- the characters are selected. Without duplicates, the characters selected
-- are all the remaining characters in the string. With duplicates, the
-- characters selected have to be unique remaining characters in the
-- string. To do this we keep the remainder as a map of character to count.
-- Then, we can iterate over the keys and filter out the empty counts to
-- get the unique remaining characters.
permutations' :: String -> [String]
permutations' = go [] "" . buildMapAndLen
  where
    buildMapAndLen s = (foldl' incrOrInsert H.empty s, length s)
    incrOrInsert acc c
        | H.member c acc = H.adjust (+ 1) c acc
        | otherwise      = H.insert c 1 acc
    go results suffix (_, 0) = suffix:results
    go results !suffix (remainder, remaining) =
        foldl' foldChoice results choices
      where
        withRemainder c = (c, H.adjust (subtract 1) c remainder)
        choices = fmap (withRemainder . fst)
                . filter ((> 0) . snd)
                . H.toList
                $ remainder
        foldChoice results (c, remainder) =
            go results (c:suffix) (remainder, remaining - 1)
