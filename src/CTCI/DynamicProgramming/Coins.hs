{-# LANGUAGE BangPatterns #-}
module CTCI.DynamicProgramming.Coins where

-- Given an infinite number of quarters (25 cents), dimes (10 cents),
-- nickels (5 cents), and pennies (1 cent), write code to calculate the
-- number of ways of representing n cents.

import Data.Array
import Data.Foldable

-- | For each coin type add the results of all the choices of how many of
-- that coin type to use.
coins :: Int -> Int
coins = go [25, 10, 5, 1] 0
  where
    go [] build 0 = build + 1
    go [] build _ = build
    go (c:types) !build n = foldl' addResults build [0..n `div` c]
      where addResults build num = go types build (n - c * num)

-- | DP version of coins. The coins have to be represented as
-- a vector and an index in order to store it in a table.
coins' :: Int -> Int
coins' n = table ! (0, n)
  where
    types = listArray (0, 3) [25, 10, 5, 1]
    lastIndex = snd $ bounds types

    build i n = foldl' addResults 0 [0..n `div` c]
      where
        c = types ! i
        addResults acc num = acc + get (i + 1) (n - c * num)

    get i 0 | i > lastIndex          = 1
    get i n | n >= 0, i <= lastIndex = table ! (i, n)
    get _ _                          = 0

    bnds = ((0, 0), (3, n))
    table = listArray bnds [build i n' | (i, n') <- range bnds]
