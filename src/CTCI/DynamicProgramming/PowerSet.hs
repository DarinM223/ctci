{-# LANGUAGE BangPatterns #-}
module CTCI.DynamicProgramming.PowerSet where

import Data.Bits

-- Write a method to return all subsets of a set.

-- | Recursively get the next subsets, clone the subsets and add
-- the current element to each subset in the cloned version and merge it
-- with the original subsets.
getSubsets :: [Int] -> [[Int]]
getSubsets [] = [[]]
getSubsets (e:es) = nextSubsets ++ moreSubsets
  where
    nextSubsets = getSubsets es
    moreSubsets = fmap (e:) nextSubsets

-- | Walks from every integer from 0 to 2^n - 1 and converts each integer
-- into a subset. The conversion is done by treating the
-- integer as a bitstring. If a bit in the integer is 1, the element at
-- the corresponding index of the list is to be kept in the subset.
getSubsetsTwo :: [Int] -> [[Int]]
getSubsetsTwo l = fmap (convertToSubset l) [0..max - 1]
  where
    max = shiftL 1 $ length l

    convertToSubset :: [Int] -> Int -> [Int]
    convertToSubset [] _      = []
    convertToSubset (e:es) !n = t'
      where
        toggled = n .&. 1 == 1
        t = convertToSubset es $ shiftR n 1
        t' | toggled   = e:t
           | otherwise = t
