{-# LANGUAGE FlexibleContexts #-}
module CTCI.DynamicProgramming.PaintFill where

-- Implement the "paint fill" function that one might see on many image
-- editing programs. That is, given a screen (represented by
-- a two-dimensional array of colors), a point, and a new color, fill in
-- the surrounding area until the color changes  from the original color.

import Control.Monad
import Data.Array.MArray
import Data.Array.IO

type Color = (Int, Int, Int)

-- | Save the different color in the first position, then do a DFS from
-- that position and if that color was the same different color then
-- change it to the new color.
paintFill :: (MArray a Color m)
          => a (Int, Int) Color
          -> (Int, Int)
          -> Color
          -> m ()
paintFill arr pos color = do
    otherColor <- readArray arr pos
    when (otherColor /= color) $ go arr pos otherColor color
  where
    go arr pos@(r, c) otherColor color = do
        (_, endPos) <- getBounds arr
        when (inBounds endPos pos) $ do
            posColor <- readArray arr pos
            when (posColor == otherColor) $ do
                writeArray arr pos color
                go arr (r + 1, c) otherColor color
                go arr (r - 1, c) otherColor color
                go arr (r, c + 1) otherColor color
                go arr (r, c - 1) otherColor color

inBounds :: (Int, Int) -- ^ End row and end column
         -> (Int, Int) -- ^ Position
         -> Bool
inBounds (er, ec) (r, c) = r >= 0 && r <= er && c >= 0 && c <= ec

testPaintArray :: (MArray a Color m) => m (a (Int, Int) Color)
testPaintArray = newListArray ((0, 0), (3, 3))
    [ g, r, r, g
    , r, g, g, r
    , r, g, g, r
    , g, r, r, g
    ]
  where
    r = (255, 0, 0)
    g = (0, 255, 0)

testPaint :: IO ()
testPaint = do
    arr <- testPaintArray :: IO (IOArray (Int, Int) Color)
    paintFill arr (1, 1) (0, 0, 255)
    elems <- getElems arr
    let elems' = splitEvery 4 $ fmap showColor elems
    mapM_ print elems'
  where
    showColor (255, 0, 0) = 'R'
    showColor (0, 255, 0) = 'G'
    showColor (0, 0, 255) = 'B'
    showColor _           = '?'

    splitEvery _ [] = []
    splitEvery n xs = as:splitEvery n bs
      where (as, bs) = splitAt n xs
