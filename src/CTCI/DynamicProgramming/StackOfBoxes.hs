module CTCI.DynamicProgramming.StackOfBoxes where

-- You have a stack of n boxes, with widths wi, heights hi, and depths di.
-- The boxes cannot be rotated and can only be stacked on top of one another
-- if each box in the stack is strictly larger than the box above it in
-- width, height, and depth. Implement a method to compute the height of
-- the tallest possible stack. The height of a stack is the sum of the
-- heights of each box.

import Data.List
import Data.Array

import qualified Data.IntMap as IM

data Box = Box
    { _width  :: Int
    , _height :: Int
    , _depth  :: Int
    } deriving (Show, Eq)

instance Ord Box where
    (<=) a b = _height b <= _height a

canBeAbove :: Box -> Box -> Bool
canBeAbove top bottom = _width bottom > _width top &&
    _height bottom > _height top && _depth bottom > _depth top

tallestStack :: [Box] -> Int
tallestStack boxes = foldl' (\acc -> max acc . (tallestHeight !)) 0
                   $ range (bounds 0)
  where
    bounds start = (start, length boxes - 1)
    sorted = listArray (bounds 0) $ sort boxes
    go boxes i = (+ _height bottom)
               . foldl' (\acc -> max acc . (tallestHeight !) . snd) 0
               . filter ((`canBeAbove` bottom) . fst)
               . fmap (\i -> (boxes ! i, i))
               $ range (bounds (i + 1))
      where
        bottom = boxes ! i
    tallestHeight = listArray (bounds 0) [go sorted i | i <- range (bounds 0)]

tallestStack' :: [Box] -> Int
tallestStack' boxes = go sorted Nothing 0 IM.empty
  where
    bounds = (0, length boxes - 1)
    sorted = listArray bounds $ sort boxes
    go boxes bottom i map
        | not $ inRange bounds i = 0
        | otherwise              = max heightWith heightWithout
      where
        bottom' = boxes ! i

        calcHeightWith = case IM.lookup i map of
            Just v  -> (map, v)
            Nothing -> (IM.insert i v' map, v')
          where
            v' = go boxes (Just bottom') (i + 1) map + _height bottom'

        (map', heightWith) = case bottom of
            Just b | bottom' `canBeAbove` b -> calcHeightWith
            Nothing                         -> calcHeightWith
            _                               -> (map, 0)
        heightWithout = go boxes bottom (i + 1) map'

testBoxesSort :: [Box]
testBoxesSort = sort testBoxes
  where
    initBox = Box { _width  = 10
                  , _height = 0
                  , _depth  = 10 }
    setHeight (box, height) = box { _height = height }
    testBoxes = take 20 . fmap setHeight . zip (repeat initBox) $ cycle [0..5]

testTallestStack :: ([Box] -> Int) -> Int
testTallestStack f = f
    [ Box 10 10 10
    , Box 10 11 11
    , Box 11 11 12
    , Box 9 9 9
    , Box 1 1 1
    , Box 1 9 1
    ]
