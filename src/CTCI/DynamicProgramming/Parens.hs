{-# LANGUAGE BangPatterns #-}
module CTCI.DynamicProgramming.Parens where

-- Implement an algorithm to print all valid (e.g. properly opened and
-- closed) combinations of n pairs of parenthesis.
--
-- Example:
-- Input: 3
-- Output: ((())), (()()), (())(), ()(()), ()()()

-- | Builds suffix string character by character. Main difference from the
-- permutation questions is that it breaks out of invalid recursive calls.
-- A call is invalid if there are negative right parenthesis or there are
-- less left parenthesis than right parenthses (since all parenthesis need
-- to be closed).
parens :: Int -> [String]
parens n = go [] "" n n
  where
    go results suffix 0 0 = suffix:results
    go !results !suffix left right
        | right < 0 || left < right = results
        | otherwise                 = results''
      where
        results' = go results ('(':suffix) (left - 1) right
        results'' = go results' (')':suffix) left (right - 1)
