{-# LANGUAGE BangPatterns #-}
module CTCI.DynamicProgramming.BooleanEvaluation where

import Control.Monad.ST
import Data.Foldable (foldlM)
import qualified Data.HashTable.ST.Basic as H

-- Given a boolean expression consisting of the symbols 0 (false),
-- 1 (true), & (AND), | (OR), and ^ (XOR), and a desired boolean value
-- result, implement a function to count the number of ways of
-- parenthesizing the expression such that it evaluates to result. The
-- expression should be fully parenthesized (e.g., (0)^(1)) but not
-- extraneously (e.g., (((0))^(1))).
--
-- Example:
-- countEval("1^0|0|1", false) -> 2
-- countEval("0&0&0&1^1|0", true) -> 10

-- | Accumulates all of the ways to get the boolean when splitting at an operator.
--
-- Example: "1^0|0|1"
--
-- Can be split into:
-- (1, ^, 0|0|1), (1^0, |, 0|1), (1^0|0, |, 1)
--
-- For each split, you then calculate the left and right sides recursively
-- with the conditions both true and false, and use those values to get the total
-- amount at the condition. Finally you accumulate all the total amounts
-- for each split to get the result.
countEval :: String -> Bool -> Int
countEval "" _        = 0
countEval ['0'] False = 1
countEval ['1'] True  = 1
countEval [_] _       = 0
countEval s cond      = go 0 1 (length s) s cond
  where
    go !build i len s cond
        | i >= len  = build
        | otherwise = go (build + result) (i + 2) len s cond
      where
        (l, op, r) = case splitAt i s of
            (l, op:r) -> (l, op, r)
            _         -> error "Invalid location"
        lTrue = countEval l True
        lFalse = countEval l False
        rTrue = countEval r True
        rFalse = countEval r False
        total = (lTrue + lFalse) * (rTrue + rFalse)
        totalTrue = case op of
            '&' -> lTrue * rTrue
            '|' -> lTrue * rTrue + lFalse * rTrue + lTrue * rFalse
            '^' -> lTrue * rFalse + lFalse * rTrue
            _   -> error "Invalid op"
        result = if cond then totalTrue else total - totalTrue

-- | Memoized version of countEval.
countEval' :: String -> Bool -> Int
countEval' s cond = runST $ do
    h <- H.new
    go s cond h
  where
    go :: String -> Bool -> H.HashTable s String Int -> ST s Int
    go s cond h = do
        cached <- H.lookup h (show cond ++ s)
        case s of
            ""                    -> pure 0
            "0" | not cond        -> pure 1
            "1" | cond            -> pure 1
            [_]                   -> pure 0
            _ | Just v <- cached  -> pure v
            _                     -> do
                res <- foldlM (addSplit s cond h) 0 [1, 3..length s - 1]
                H.insert h (show cond ++ s) res
                return res

    addSplit :: String
             -> Bool
             -> H.HashTable s String Int
             -> Int
             -> Int
             -> ST s Int
    addSplit s cond h build i = do
        let (l, op, r) = case splitAt i s of
                (l, op:r) -> (l, op, r)
                _         -> error "Invalid location"
        lTrue <- go l True h
        lFalse <- go l False h
        rTrue <- go r True h
        rFalse <- go r False h
        let total = (lTrue + lFalse) * (rTrue + rFalse)
            totalTrue = case op of
                '&' -> lTrue * rTrue
                '|' -> lTrue * rTrue + lFalse * rTrue + lTrue * rFalse
                '^' -> lTrue * rFalse + lFalse * rTrue
                _   -> error "Invalid op"
            result = if cond then totalTrue else total - totalTrue
        return $ build + result
