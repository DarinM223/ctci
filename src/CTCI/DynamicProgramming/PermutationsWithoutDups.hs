{-# LANGUAGE BangPatterns #-}
module CTCI.DynamicProgramming.PermutationsWithoutDups where

-- Write a method to compute all permutations of a string of unique
-- characters.

import Data.Foldable (foldl')
import Data.Maybe (catMaybes)

-- | Build permutations by applying a character to
-- the permutations of (n - 1) characters.
--
-- The character is applied by inserting it into every possible position in
-- every string.
permutations' :: String -> [String]
permutations' []     = [""]
permutations' (c:cs) = permutations' cs >>= insertEverywhere c
  where
    insertEverywhere c p = foldl' insert [] [0..length p]
      where
        insert acc i = (left ++ [c] ++ right):acc
          where (left, right) = splitAt i p

-- | Build strings one character at a time. Each time a character is to be
-- picked it recurses for every unpicked character. Once a character is chosen
-- it is removed from the remainder so that it can't be chosen again.
-- Once there are no more characters left that permutation is added to the
-- results.
--
-- Example: "abc"
--
--        "ba"---"cba"
--       /
--     "a"---"ca"---"bca"
--     /
--    /    "ab"---"cab"
--   /    /
-- ""---"b"
--   \    \
--    \    "cb"---"acb"
--     \
--     "c"---"ac"---"bac"
--       \
--        "bc"---"abc"
--
-- Results: "cba", "bca", "cab", "acb", "bac", "abc"
permutations'' :: String -> [String]
permutations'' = go [] ""
  where
    -- Builds string backwards so currently building string is named
    -- "suffix" instead of "prefix".
    go results suffix ""         = suffix:results
    go results !suffix remainder = foldl' foldChoice results choices
      where
        separateMiddle (left, e:right) = Just (e, left ++ right)
        separateMiddle _               = Nothing
        choices = catMaybes
                . fmap (separateMiddle . flip splitAt remainder)
                $ [0..length remainder - 1]
        foldChoice results (e, remainder) = go results (e:suffix) remainder
