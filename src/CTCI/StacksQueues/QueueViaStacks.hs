module CTCI.StacksQueues.QueueViaStacks where

import Control.Monad.Trans.State
import Data.Maybe

-- Implement a MyQueue class which implements a queue using two stacks.

data StackQueue a = StackQueue
    { _stack1 :: [a]
    , _stack2 :: [a]
    }

newStackQueue :: StackQueue a
newStackQueue = StackQueue { _stack1 = [], _stack2 = [] }

enqueue :: a -> StackQueue a -> StackQueue a
enqueue e queue = queue { _stack2 = e:_stack2 queue }

dequeue q@StackQueue{ _stack1 = e:s }              = (Just e, q { _stack1 = s })
dequeue q@StackQueue{ _stack1 = [], _stack2 = [] } = (Nothing, q)
dequeue q@StackQueue{ _stack1 = [] }               = dequeue $ shift q

peek q@StackQueue{ _stack1 = e:s }              = (Just e, q { _stack1 = e:s })
peek q@StackQueue{ _stack1 = [], _stack2 = [] } = (Nothing, q)
peek q@StackQueue{ _stack1 = [] }               = peek $ shift q

shift :: StackQueue a -> StackQueue a
shift q = q { _stack1 = reverse $ _stack2 q
            , _stack2 = []
            }

-- Should return [1, 1, 1, 2, 3, 4, 5, 6].
testStackQueue :: [Int]
testStackQueue = fst $ flip runState newStackQueue $ do
    modify $ enqueue 1
    modify $ enqueue 2
    modify $ enqueue 3
    pe1 <- state peek
    pe2 <- state peek
    e1 <- state dequeue
    e2 <- state dequeue
    modify $ enqueue 4
    modify $ enqueue 5
    e3 <- state dequeue
    modify $ enqueue 6
    e4 <- state dequeue
    e5 <- state dequeue
    e6 <- state dequeue
    return $ catMaybes [pe1, pe2, e1, e2, e3, e4, e5, e6]
