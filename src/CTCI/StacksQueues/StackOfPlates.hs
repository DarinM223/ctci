{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE LambdaCase #-}
module CTCI.StacksQueues.StackOfPlates where

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State
import Data.Foldable (foldl')

-- Imagine a (literal) stack of plates. If the stack gets too high, it
-- might topple. Therefore, in real life, we would likely start a new stack
-- when the previous stack exceeds some threshold. Implement a data
-- structure SetOfStacks that mimics this. SetOfStacks should be composed
-- of several stacks and should create a new stack once the previous one
-- exceeds capacity. SetOfStacks.push() and SetOfStacks.pop() should behave
-- identically to a single stack (that is, pop() should return the same
-- values as it would if there were just a single stack).
--
-- Follow up:
-- Implement a function popAt(int index) which performs a pop operation on
-- a single stack.

data Stack a = Stack
    { _stack  :: [a]
    , _length :: Int
    } deriving (Show, Eq)

newStack :: [a] -> Stack a
newStack l = Stack { _stack = l, _length = length l }

pushStack :: a -> Stack a -> Stack a
pushStack e s = s { _stack = e:_stack s, _length = _length s + 1 }

popStack :: Stack a -> (a, Stack a)
popStack s@Stack{ _stack = e:es, _length = len } = (e, s')
  where
    s' = s { _stack = es, _length = len - 1 }
popStack _ = error "Stack is empty"

data SetOfStacks a = SetOfStacks
    { _stacks   :: [Stack a]
    , _capacity :: Int
    } deriving (Show, Eq)

new :: Int -> SetOfStacks a
new capacity = SetOfStacks { _stacks = [], _capacity = capacity }

push :: a -> SetOfStacks a -> SetOfStacks a
push e s@SetOfStacks{ _stacks = (stack:rest) }
    | _length stack == _capacity s = s { _stacks = newStack [e]:stack:rest }
    | otherwise                    = s { _stacks = pushStack e stack:rest }
push e s = s { _stacks = [newStack [e]] }

pop :: SetOfStacks a -> (Maybe a, SetOfStacks a)
pop s@SetOfStacks { _stacks = (stack:rest) }
    | _length stack <= 0 = pop s { _stacks = rest }
    | otherwise          = (Just e, s { _stacks = consIgnoreEmpty stack' rest })
  where
    (e, stack') = popStack stack
pop s = (Nothing, s)

popAt :: Int -> SetOfStacks a -> (Maybe a, SetOfStacks a)
popAt i s@SetOfStacks{ _stacks = stacks }
    | i < length stacks, i >= 0 = (v, s { _stacks = left' ++ right' })
    | otherwise                 = (Nothing, s)
  where
    (left, right) = splitAt (length stacks - 1 - i) stacks
    (a, left') = foldl' handleStack (Nothing, []) left
    (v, right') = case right of
        stack:rest ->
            let (v, stack') = flip runState stack $ do
                    v <- state popStack
                    mapM_ (modify' . pushStack) a
                    return $ Just v
            in (v, consIgnoreEmpty stack' rest)
        _ -> (Nothing, right)

    handleStack (v, !rest) stack = (backVal, consIgnoreEmpty stack' rest)
      where
        (backVal, stack') = flip runState stack $ do
            backVal <- gets _stack >>= \case
                [] -> Nothing <$ put (newStack [])
                l  -> Just (last l) <$ put (newStack $ init l)
            mapM_ (modify' . pushStack) v
            return backVal

consIgnoreEmpty :: Stack a -> [Stack a] -> [Stack a]
consIgnoreEmpty stack stacks = case _stack stack of
    [] -> stacks
    _  -> stack:stacks

testStacks :: IO ()
testStacks = void $ flip runStateT (new 5) $ do
    mapM_ (\num -> modify' (push num)) [1..10]
    get >>= lift . print
    state (popAt 0) >>= lift . print
    get >>= lift . print
    state (popAt 1) >>= lift . print
    get >>= lift . print
    replicateM_ 3 $ state $ popAt 0
    get >>= lift . print
    replicateM_ 3 $ state $ popAt 1
    get >>= lift . print
    replicateM_ 11 $ do
        v <- state pop
        lift $ print v
