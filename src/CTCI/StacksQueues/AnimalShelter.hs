{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
module CTCI.StacksQueues.AnimalShelter where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Data.Sequence

-- An animal shelter, which holds only dogs and cats, operates on
-- a strictly "first in, first out" basis. People must adopt either the
-- "oldest" (based on arrival time) of all animals at the shelter, or they
-- can select whether they would prefer a dog or a cat (and will receive
-- the oldest animal of that type). They cannot select which specific
-- animal they would like. Create the data structures to maintain this
-- system and implement operations such as enqueue, dequeueAny, dequeueDog,
-- and dequeueCat. You may use the built-in LinkedList data structure.

class Animal a

data Dog = Dog String deriving (Show)
instance Animal Dog

data Cat = Cat String deriving (Show)
instance Animal Cat

data AnyAnimal = forall a. (Show a, Animal a) => AnyAnimal a
instance Show AnyAnimal where show (AnyAnimal a) = show a

newtype ArrivalTime = ArrivalTime Int deriving (Show, Eq, Ord, Num)

data AnimalShelter = AnimalShelter
    { _dogs :: Seq (ArrivalTime, Dog)
    , _cats :: Seq (ArrivalTime, Cat)
    , _time :: ArrivalTime
    }

newAnimalShelter :: AnimalShelter
newAnimalShelter = AnimalShelter { _dogs = Empty, _cats = Empty, _time = 0 }

enqueueDog :: Dog -> AnimalShelter -> AnimalShelter
enqueueDog dog s = s { _dogs = _dogs s :|> (_time s, dog)
                     , _time = _time s + 1 }
enqueueCat :: Cat -> AnimalShelter -> AnimalShelter
enqueueCat cat s = s { _cats = _cats s :|> (_time s , cat)
                     , _time = _time s + 1 }

dequeueAny :: AnimalShelter -> (Maybe AnyAnimal, AnimalShelter)
dequeueAny s@(AnimalShelter Empty Empty _) = (Nothing, s)
dequeueAny (AnimalShelter ((_, dog) :<| dogs) Empty time) =
    (Just (AnyAnimal dog), AnimalShelter dogs Empty time)
dequeueAny (AnimalShelter Empty ((_, cat) :<| cats) time) =
    (Just (AnyAnimal cat), AnimalShelter Empty cats time)
dequeueAny s@AnimalShelter{ _dogs = (dt, dog) :<| dogs
                          , _cats = (ct, cat) :<| cats }
    | dt < ct   = (Just (AnyAnimal dog), s { _dogs = dogs })
    | otherwise = (Just (AnyAnimal cat), s { _cats = cats })

dequeueDog :: AnimalShelter -> (Maybe Dog, AnimalShelter)
dequeueDog s@AnimalShelter{ _dogs = (_, dog) :<| dogs } =
    (Just dog, s { _dogs = dogs })
dequeueDog s@AnimalShelter{ _dogs = Empty } = (Nothing, s)

dequeueCat :: AnimalShelter -> (Maybe Cat, AnimalShelter)
dequeueCat s@AnimalShelter{ _cats = (_, cat) :<| cats } =
    (Just cat, s { _cats = cats })
dequeueCat s@AnimalShelter{ _cats = Empty } = (Nothing, s)

testAnimalShelter :: IO ()
testAnimalShelter = void $ flip runStateT newAnimalShelter $ do
    modify' $ enqueueDog $ Dog "Fido"
    modify' $ enqueueCat $ Cat "Meow"
    modify' $ enqueueDog $ Dog "Woof"
    modify' $ enqueueDog $ Dog "Pupper"
    modify' $ enqueueCat $ Cat "Nyaa"
    printDequeueAnys

    modify' $ enqueueDog $ Dog "Doggo"
    modify' $ enqueueDog $ Dog "Good Boi"
    modify' $ enqueueCat $ Cat "Kitten"
    modify' $ enqueueDog $ Dog "Bark"
    modify' $ enqueueCat $ Cat "Stray"

    printDequeueCats
    printDequeueDogs
  where
    printDequeueAnys = state dequeueAny >>= \case
        Just animal -> liftIO (print animal) >> printDequeueAnys
        Nothing     -> return ()
    printDequeueDogs = state dequeueDog >>= \case
        Just dog -> liftIO (print dog) >> printDequeueDogs
        Nothing  -> return ()
    printDequeueCats = state dequeueCat >>= \case
        Just cat -> liftIO (print cat) >> printDequeueCats
        Nothing  -> return ()
