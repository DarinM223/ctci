{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
module CTCI.StacksQueues.ThreeInOne.GrowingStack where

-- Describe how you could use a single array to implement three stacks.

import Control.Monad
import Data.Array.MArray
import Data.Foldable

data StackError = FullStack | EmptyStack

data StackInfo = StackInfo
    { _size     :: Int
    , _capacity :: Int
    , _start    :: Int
    }

inStackRange :: Int -- ^ Index to check if it is in stack range
             -> Int -- ^ Max size of the multistack
             -> StackInfo
             -> Bool
inStackRange i maxlen info
    | i < 0 || i >= maxlen = False
    | otherwise            = adjustedI >= start && adjustedI < end
  where
    start = _start info
    end = start + _capacity info
    -- i adjusted for wraparound
    adjustedI | i < start = i + maxlen
              | otherwise = i

data GrowingMultiStack arr a = GrowingMultiStack
    { _infos     :: arr Int StackInfo
    , _values    :: arr Int a
    , _numInfos  :: Int
    , _totalSize :: Int
    }

newGrowingStack :: (MArray arr a m, MArray arr StackInfo m)
                => Int -- ^ # of stacks
                -> Int -- ^ Default size of stacks
                -> a   -- ^ Default initial value
                -> m (GrowingMultiStack arr a)
newGrowingStack numStacks defaultSize defaultValue = do
    infos <- newListArray (0, numStacks - 1) infoList
    let totalSize = numStacks * defaultSize
    values <- newArray (0, totalSize - 1) defaultValue
    return GrowingMultiStack
        { _infos     = infos
        , _values    = values
        , _numInfos  = numStacks
        , _totalSize = totalSize
        }
  where
    infoList = do
        i <- [0..numStacks - 1]
        return StackInfo
            { _size     = 0
            , _capacity = defaultSize
            , _start    = i * defaultSize
            }

pushGrowing :: (MArray arr a m, MArray arr StackInfo m)
            => Int -- ^ Stack index
            -> a   -- ^ Element to push
            -> GrowingMultiStack arr a
            -> m (Either StackError ())
pushGrowing i e stacks = do
    size <- fmap totalSize . getElems . _infos $ stacks
    if size == _totalSize stacks
        then return $ Left FullStack
        else do
            info <- readArray (_infos stacks) i
            when (_size info == _capacity info) $ expand i stacks
            writeArray (_infos stacks) i info { _size = _size info + 1 }
            let index         = _start info + _size info
                adjustedIndex = adjustIndex index (_totalSize stacks)
            writeArray (_values stacks) adjustedIndex e
            return $ Right ()

popGrowing :: (MArray arr a m, MArray arr StackInfo m)
           => Int -- ^ Stack index
           -> GrowingMultiStack arr a
           -> m (Either StackError a)
popGrowing i stacks = do
    info <- readArray (_infos stacks) i
    if _size info == 0
        then return $ Left EmptyStack
        else do
            let size      = _totalSize stacks
                lastIndex = adjustIndex (_start info + _size info - 1) size
            value <- readArray (_values stacks) lastIndex
            writeArray (_infos stacks) i info { _size = _size info - 1 }
            return $ Right value

expand :: (MArray arr a m, MArray arr StackInfo m)
       => Int -- ^ Stack index
       -> GrowingMultiStack arr a
       -> m ()
expand i stacks = do
    shift ((i + 1) `rem` _numInfos stacks) stacks -- Shift the next stack

    info <- readArray (_infos stacks) i
    writeArray (_infos stacks) i info { _capacity = _capacity info + 1 }

shift :: (MArray arr a m, MArray arr StackInfo m)
      => Int -- ^ Stack index
      -> GrowingMultiStack arr a
      -> m ()
shift i stacks = do
    -- If stack is full shift next stack and increment capacity
    info <- readArray (_infos stacks) i >>= \case
        info | _size info >= _capacity info -> do
            shift ((i + 1) `rem` _numInfos stacks) stacks
            return info { _capacity = _capacity info + 1 }
        info -> return info

    let size      = _totalSize stacks
        lastIndex = adjustIndex (_start info + _capacity info - 1) size
    shiftStack lastIndex info size stacks
    writeArray (_infos stacks) i info
        { _start    = adjustIndex (_start info + 1) size
        , _capacity = _capacity info - 1
        }
  where
    shiftStack index info size stacks
        | inStackRange index size info = do
            let prevIndex = adjustIndex (index - 1) size
            prevValue <- readArray (_values stacks) prevIndex
            writeArray (_values stacks) index prevValue
            shiftStack prevIndex info size stacks
        | otherwise = return ()

totalSize :: [StackInfo] -> Int
totalSize = foldl' (\s -> (+ s) . _size) 0

adjustIndex :: Int -- ^ Index to adjust
            -> Int -- ^ Size of the array
            -> Int -- ^ Index adjusted for wrap around
adjustIndex i size =
    -- Normally i % size would be good enough but
    -- modulus can return a negative value, so we do
    -- ((i % size) + size) % size.
    --
    -- Example: (-11 % 5) returns -1 instead of 4.
    -- (-1 + 5) % 5 = 4
    --
    -- On the other hand positive values will stay the same:
    -- (4 + 5) % 5 = 4
    ((i `rem` size) + size) `rem` size
