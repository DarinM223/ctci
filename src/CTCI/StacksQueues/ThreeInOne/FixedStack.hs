{-# LANGUAGE FlexibleContexts #-}
module CTCI.StacksQueues.ThreeInOne.FixedStack where

-- Describe how you could use a single array to implement three stacks.

import Data.Array.MArray

data StackError = FullStack | EmptyStack

data FixedMultiStack arr a = FixedMultiStack
    { _numStacks :: Int
    , _capacity  :: Int
    , _values    :: arr Int a
    , _sizes     :: arr Int Int
    }

newFixedStack :: (MArray arr a m, MArray arr Int m)
              => Int -- ^ Max stack capacity
              -> Int -- ^ # of stacks
              -> a   -- ^ Default value to initialize with
              -> m (FixedMultiStack arr a)
newFixedStack capacity numStacks defaultValue = do
    values <- newArray (0, numStacks * capacity - 1) defaultValue
    sizes <- newArray (0, numStacks - 1) 0
    return FixedMultiStack
        { _numStacks = numStacks
        , _capacity  = capacity
        , _values    = values
        , _sizes     = sizes
        }

pushFixed :: (MArray arr a m, MArray arr Int m)
          => Int -- ^ Stack index
          -> a   -- ^ Element to push to stack
          -> FixedMultiStack arr a
          -> m (Either StackError ())
pushFixed i e stack = do
    size <- readArray (_sizes stack) i
    if size >= _capacity stack
        then return $ Left FullStack
        else do
            let top = topIdx i (_capacity stack) (size + 1)
            writeArray (_values stack) top e
            writeArray (_sizes stack) i (size + 1)
            return $ Right ()

popFixed :: (MArray arr a m, MArray arr Int m)
         => Int -- ^ Stack index
         -> FixedMultiStack arr a
         -> m (Either StackError a)
popFixed i stack = do
    size <- readArray (_sizes stack) i
    if size < 1
        then return $ Left FullStack
        else do
            let top = topIdx i (_capacity stack) size
            value <- readArray (_values stack) top
            writeArray (_sizes stack) i (size - 1)
            return $ Right value

topIdx :: Int -- ^ Stack index
       -> Int -- ^ Stack capacity
       -> Int -- ^ Size of stack
       -> Int -- ^ Index of top element in stack
topIdx i capacity size = i * capacity + size - 1
