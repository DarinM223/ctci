module CTCI.StacksQueues.StackMin where

-- How would you design a stack which, in addition to push and pop, has
-- a function min which returns the minimum element? Push, pop, and min
-- should all operate in O(1) time.

data MinStack a = MinStack { _stack :: [(a, a)] }

push :: (Ord a) => a -> MinStack a -> MinStack a
push e s@MinStack{ _stack = (_, currMin):_ }
    = s { _stack = (e, min):_stack s }
  where
    min | e < currMin = e
        | otherwise   = currMin
push e s = s { _stack = [(e, e)] }

pop :: MinStack a -> (Maybe a, MinStack a)
pop s@MinStack{ _stack = (e, _):rest } = (Just e, s { _stack = rest })
pop s                                  = (Nothing, s)

min :: MinStack a -> Maybe a
min MinStack{ _stack = (_, min):_ } = Just min
min _                               = Nothing

-- | More space efficient in cases of lots of
-- elements but only one min.
data MinStack2 a = MinStack2
    { _values :: [a]
    , _mins   :: [a]
    }

push2 :: (Ord a) => a -> MinStack2 a -> MinStack2 a
push2 e s@MinStack2{ _mins = currMin:_ }
    | e <= currMin = s { _values = e:_values s
                       , _mins   = e:_mins s }
    | otherwise    = s { _values = e:_values s }
push2 e s = s { _values = [e], _mins = [e] }

pop2 :: (Eq a) => MinStack2 a -> (Maybe a, MinStack2 a)
pop2 s@MinStack2{ _values = v:vs, _mins = m:ms }
    | v == m    = (Just v, s { _values = vs, _mins = ms })
    | otherwise = (Nothing, s { _values = vs })
pop2 s = (Nothing, s)

min2 :: MinStack2 a -> Maybe a
min2 MinStack2{ _mins = min:_ } = Just min
min2 _                          = Nothing
