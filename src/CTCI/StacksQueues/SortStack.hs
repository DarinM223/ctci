module CTCI.StacksQueues.SortStack where

-- Write a program to sort a stack such that the smallest items are on the
-- top. You can use an additional temporary stack, but you may not copy the
-- elements into any other data structure (such as an array). The stack
-- supports the following operations: push, pop, peek, and isEmpty.

sortStack :: (Ord a)
          => [a] -- ^ Temporary stack
          -> [a] -- ^ Stack to sort
          -> [a] -- ^ The sorted stack
sortStack [] (e:s) = sortStack [e] s
sortStack temp []  = temp
sortStack (t:ts) (e:es)
    | t < e     = insertInTemp (t:ts) es e
    | otherwise = sortStack (e:t:ts) es
  where
    -- Inserts a new element into the temporary stack in order by
    -- moving elements less than it into the other stack.
    insertInTemp [] es newTempElem = sortStack [newTempElem] es
    insertInTemp (t:ts) es newTempElem
        | t < newTempElem = insertInTemp ts (t:es) newTempElem
        | otherwise       = sortStack (newTempElem:t:ts) es

-- Example:
-- [5, 4, 1, 6, 2] -> [1, 2, 4, 5, 6]
--
-- Temp:  []
-- Stack: 5 4 1 6 2
--
-- Temp:  5
-- Stack: 4 1 6 2
--
-- Temp:  1 4 5
-- Stack: 6 2
--
-- 1 < 6!
--
-- While < 6 pop from temp and push onto stack:
--
-- Temp:  []
-- Stack: 5 4 1 2
--
-- Push 6 on temp stack:
--
-- Temp:  6
-- Stack: 5 4 1 2
--
-- Keep going for 5, 4, and 1:
--
-- Temp:  1 4 5 6
-- Stack: 2
--
-- 1 < 2!
--
-- While < 2 pop from temp and push onto stack:
--
-- Temp:  4 5 6
-- Stack: 1
--
-- Push 2 on temp stack:
--
-- Temp:  2 4 5 6
-- Stack: 1
--
-- Keep going for 1:
--
-- Temp:  1 2 4 5 6
-- Stack: []
--
-- When stack is empty, return temp
